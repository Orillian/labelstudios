@extends('layouts.app')

@section('content')
    @include('layouts.header')
    @include('layouts.nav')

    <h3 class="page-title mt-3">Projects</h3>

    <section class="project mt-3 border-bottom pb-3">
        <h4><a href="https://bitbucket.org/Orillian/labelstudios/">labelStudios.ca</a></h4>
        <p>: The code for this site.</p>
    </section>

    <section class="project mt-3 border-bottom pb-3">
        <h4>BikeShare</h4>
        <p>: A bike lending application built for our local tourist center. No project files are available at this time.
            Built in PHP w/ laravel 5.x, and JQuery.</p>
        <p>This application was developed to provide an interface to allow the lending of bikes from a number of
            locations across the city to be done through a single interface that would allow the tracking of each bike.
            Each bike would be assigned a home, and could be lent to borrowers, have it's status updated, as well as
            maintain stats and notes on each. Id and waiver checks could be validated for each borrower and
            maintained. </p>
        <div class="col-12">
            <div class="row">
                <a href="#" class="project-image col-4">
                    <img src="{{asset('images/Screenshot_2018-10-29 BikeShare.png')}}"
                         alt="BikeShare Image #1 - Splash Page."
                         class="img-thumbnail rounded-0">
                </a>
                <a href="#" class="project-image col-4">
                    <img src="{{asset('images/Screenshot_2018-10-29 BikeShare-Bikes.png')}}"
                         alt="BikeShare Image #2 - Home page."
                         class="img-thumbnail rounded-0">
                </a>
                <a href="#" class="project-image col-4">
                    <img src="{{asset('images/Screenshot_2018-10-29 BikeShare-Borrow.png')}}"
                         alt="BikeShare Image #3 - Borrow Modal."
                         class="img-thumbnail rounded-0">
                </a>
            </div>
        </div>
    </section>

    {{--<section class="project mt-3 border-bottom pb-3">--}}
    {{--<h4><a href="https://thehobsons.ca">theHobsons.ca</a></h4>: A place for Hobsons to meet.--}}
    {{--<div class="col-12">--}}
    {{--<div class="row">--}}
    {{--<img src="#" alt="#"--}}
    {{--class="rounded img-thumbnail col-4">--}}
    {{--<img src="#" alt="#"--}}
    {{--class="img-thumbnail col-4">--}}
    {{--<img src="#" alt="#"--}}
    {{--class="img-thumbnail col-4">--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</section>--}}

    {{--<section class="project mt-3 border-bottom pb-3">--}}
    {{--<h4><a href="#">Monarch of the Savage Lands</a></h4>: SUD engine.--}}
    {{--<div class="col-12">--}}
    {{--<div class="row">--}}
    {{--<img src="#" alt="#"--}}
    {{--class="rounded img-thumbnail col-4">--}}
    {{--<img src="#" alt="#"--}}
    {{--class="img-thumbnail col-4">--}}
    {{--<img src="#" alt="#"--}}
    {{--class="img-thumbnail col-4">--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</section>--}}

    <section class="project mt-3 border-bottom pb-3">
        <h4><a href="https://github.com/Orillian/php_tineye">TinEye Php Library</a></h4>
        <p>: This is a simple php library that uses the TinEye API. Accesses the API with Curl.</p>
    </section>
@endsection