<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'labelStudios') }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
<canvas id="crt_effect"></canvas>
<div id="app">
    <pre class="background-code disabled">
        {{ $preContent ?? "echo 'Hello World!';"}}
    </pre>

    @stack('modals')
    @include('layouts.image-modal')

    <main class="container">
        @yield('content')
    </main>
</div>
<p class="π">π</p>
</body>
@include('layouts.scripts')
</html>
