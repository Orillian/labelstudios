<nav class="navbar navbar-expand-lg px-0 navbar-dark pt-0 mb-3">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link" href="{{!Request::is('/') ? route('home') : '#'}}">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{!Request::is('about') ? route('about') : '#'}}">About</a>
        </li>
        <li class="nav-item collapse">
            <a class="nav-link" href="{{!Request::is('blog') ? route('blog') : '#'}}">Blog</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{!Request::is('contact') ? route('contact') : '#'}}">Contact</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{!Request::is('projects') ? route('projects') : '#'}}">Projects</a>
        </li>
    </ul>
</nav>