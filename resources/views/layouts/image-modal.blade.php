<div id="image-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content rounded-0">
            <div class="modal-body p-0">
                <img class="img-fluid mx-auto d-block" src="" alt="Project Image" class="close" data-dismiss="modal" aria-label="Close">
            </div>
        </div>
    </div>
</div>