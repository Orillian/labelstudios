@extends('layouts.app')

@section('content')
    @include('layouts.header')
    @include('layouts.nav')
    <h3 class="page-title mt-3">Contact</h3>

    <p>Contact me! After all that's why you're here!</p>

    <div class="form-group">
        <label for="labelStudios-subject">Subject</label>
        <input type="text" class="form-control" name="subject" placeholder="What would you like to talk about?">
    </div>

    <div class="form-group">
        <label for="labelStudios-message">Message</label>
        <textarea class="form-control" name="message" rows="8" placeholder="Lets get into the details!"></textarea>
    </div>

    <div class="form-group">
        <label for="labelStudios-email">Email</label>
        <input type="text" class="form-control" name="email" placeholder="email...">
    </div>

    ...I'll let you submit your messages soon!

@endsection