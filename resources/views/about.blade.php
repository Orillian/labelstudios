@extends('layouts.app')

@section('content')
    @include('layouts.header')
    @include('layouts.nav')

    <h3 class="page-title mt-3">About</h3>

    <p>Richard Hobson, that's the name I was given and it's the one I still use most days. I'm {{$my_age ?? 'π'}} and a
        Senior Software Developer living in Medicine Hat, Alberta Canada.</p>

    <p>I've been working for the past {{$year ?? 'π'}} years as a developer for
        <a href="https://{{$employer_web_address ?? 'π'}}">{{$current_employer ?? 'π'}}</a> building web applications
        for the auto industry.</p>

    <p>In my off time I develop software for fun and for others. I'm always looking to build things that people can use
        to improve or simplify their day to day lives. I also enjoy games. Games of all types; board games, card games,
        retro console and PC. I also have a passion for 3D printers. Hacking away on models and tinkering/modding them.
        Last but not least I love the outdoors. Fishing, hiking, biking and camping... not that I get as much
        opportunity to do these things as I'd like!</p>

    <p>This is not the most informative about page you've read is it? Kinda short. Meh, I'll add more later.</p>


@endsection