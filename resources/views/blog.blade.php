@extends('layouts.app')

@section('content')
    @include('layouts.header')
    @include('layouts.nav')

    <h3 class="page-title mt-3">Blog</h3>

    <p>This section will be for any articles that I happen to write. This will be hidden until such time as it is needed.</p>
@endsection