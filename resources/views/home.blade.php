@extends('layouts.app')

@section('content')
    @include('layouts.header')
    @include('layouts.nav')
    <p>echo 'Hello World!';</p>

    <p>This site is intended as a platform to showcase some of my projects and ideas.</p>

    <p>I've kept the idea of labelStudios alive for many years. It's always been a part of me, but until now has not had
        a fully realized space to reside.</p>

    <p>What labelStudios is, continues to shift and change, but no matter the focus of my life it remains a space
        for my creative side to reside. Even during it's long winter as nothing more than a folder on my hard drive.</p>

    <p>As I have the time, this space will grow to contain those snippets.</p>

    <p>I plan to showcase projects in web development, art, design and maybe even a few ideas that I feel are worth
        sharing.</p>

    <p>Regards,</p>

    <label class="signature">Richard O. Hobson</label>

@endsection