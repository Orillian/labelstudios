/**
* Font Awesome 5
*/
import {library, dom} from '@fortawesome/fontawesome-svg-core'
import {faInfo} from '@fortawesome/pro-regular-svg-icons/faInfo';

library.add(faInfo);

dom.watch();