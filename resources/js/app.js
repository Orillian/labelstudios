/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
// require('./font-awesome');

// window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component('example-component', require('./components/ExampleComponent.vue'));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// const app = new Vue({
//     el: '#app'
// });

(function cursor_blink() {
    $('.cursor').fadeOut(1000).fadeIn(1000, cursor_blink);
})();


(function image_modal() {
    $('.project-image').on('click', function (e) {
        e.preventDefault();
        let image_modal = $('#image-modal');
        image_modal.find('img').attr('src', $(this).find('img').attr('src'));
        image_modal.modal('show');
    })
})();

(function () {
    $('.π').on('click', function () {
        let image_modal = $('#image-modal');
        image_modal.find('img').attr('src', '/images/732duanest.gif');
        image_modal.modal('show');
    })
})();

(function crt_effect() {
    let canvas = document.getElementById('crt_effect');

    if (canvas) {
        let chance = Math.random();

        //add our random colour to the body as a class.
        let style = 'white';
        if (chance <= 0.05) {
            style = 'green';
        } else if (chance >= 0.95) {
            style = 'orange';
        }
        if(style !== 'white'){
        document.body.classList.add('crt_' + style);
        }

        canvas.width = document.documentElement.clientWidth;
        canvas.height = document.documentElement.clientHeight;
        let ctx = canvas.getContext('2d');

        if (localStorage.getItem('overlay.' + style) !== null) {
            let overlay = localStorage.getItem('overlay.' + style);
            let img = new Image;
            img.src = overlay;
            img.onload = function () {
                ctx.drawImage(img, 0, 0);
            };
        } else {

            let overlay_color = '#fff';
            let alpha = 0.15;
            if (style === 'green') {
                overlay_color = '#1a6215';
                alpha = 0.35;
            } else if (style === 'orange') {
                overlay_color = '#856d00';
                alpha = 0.25;
            }

            ctx.globalAlpha = alpha;
            for (let i = 0; i < canvas.height; i += 3) {
                ctx.beginPath();
                ctx.moveTo(0, i);
                ctx.lineTo(canvas.width, i);
                ctx.lineWidth = 1;
                ctx.strokeStyle = overlay_color;
                ctx.stroke();
            }
            localStorage.setItem('overlay.' + style, canvas.toDataURL());
        }
    }
})();
