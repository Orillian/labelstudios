<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\View;

class IndexController extends Controller
{


    /**
     * Show the about page.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about()
    {
        $current_employer = 'Accessible Accessories Ltd.';
        $my_age = Carbon::parse('1976-05-08')->age;
        $view = View::make('about');
        $preContent = (string)$view;
        $website = 'acc-acc.ca';
        $year = Carbon::parse('2010-05-02')->age;

        return $view->with(
            [
                'current_employer'     => $current_employer,
                'employer_web_address' => $website,
                'my_age'               => $my_age,
                'preContent'           => $preContent,
                'year'                 => $year,
            ]);
    }

    /**
     * Show the blog page.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function blog()
    {
        $view = View::make('blog');
        $preContent = (string)$view;
        return $view->with(['preContent' => $preContent]);
    }

    /**
     * Show the contact page.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contact()
    {
        $view = View::make('contact');
        $preContent = (string)$view;
        return $view->with(['preContent' => $preContent]);
    }

    /**
     * Show the home page.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home()
    {
        $view = View::make('home');
        $preContent = (string)$view;
        return $view->with(['preContent' => $preContent]);
    }

    /**
     * Show the projects page.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function projects()
    {
        $view = View::make('projects');
        $preContent = (string)$view;
        return $view->with(['preContent' => $preContent]);
    }
}
