<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;

Route::get('/', 'IndexController@home')->name('home');
Route::get('about', 'IndexController@about')->name('about');
Route::get('blog', 'IndexController@blog')->name('blog');
Route::get('contact', 'IndexController@contact')->name('contact');
Route::get('projects', 'IndexController@projects')->name('projects');